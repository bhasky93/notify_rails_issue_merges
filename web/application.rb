require_relative 'boot'
module Web
  module Application
    autoload :DatabaseUtils, 'web/application/database_utils'
    autoload :ActionsDSL, 'web/application/actions_dsl'
    autoload :BaseController, 'web/application/base_controller'
    autoload :BaseMailer, 'web/application/base_mailer'

    extend Sinatra::Delegator
    extend DatabaseUtils

    PERMITTED_CONFIGS = [:orm, :mailer_options]

    class << self
      def configure(environment, &block)
        class_eval(&block) if settings.environment.to_sym == environment
      end

      def draw_routes(&block)
        class_eval(&block)
      end

      def init(environment=nil)
        set :environment, environment.to_sym unless environment.nil?
        require_relative '../config/app'
      end

      def load_tasks
        rake_files = Dir["#{File.expand_path('../../lib/tasks', __FILE__)}/**/*.rake"]
        rake_files.each do |task_file|
          load task_file
        end
      end

      def environment
        settings.environment
      end

      private

      def set(config_name, config_value)
        return super(config_name, config_value) unless PERMITTED_CONFIGS.include?(config_name)
        send(config_name, config_value)
      end

      def mailer_options(config_value)
        BaseMailer.set_default_mailer_options(config_value)
      end
    end
  end
end

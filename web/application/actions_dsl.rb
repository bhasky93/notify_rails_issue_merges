module Web
  module Application
    module ActionsDSL
      def action(name = nil, &action_body)
        raise "Provide action body" unless block_given?
        raise "Provide action name" if name.nil?
        actions[name.to_sym] = action_body
      end

      def [](action_name)
        if actions[action_name].nil?
          raise "Given action is not present"
        else
          actions[action_name]
        end
      end

      private

      attr_accessor :actions

      def actions
        @actions ||= {}
      end
    end
  end
end

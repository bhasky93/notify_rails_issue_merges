Dir[
  "app/models/**/*.rb",
  "app/controllers/**/*.rb",
  "app/mailers/**/*.rb",
  "lib/**/*.rb"
].each do |file_path|
  require File.expand_path(file_path) rescue SQLite3::SQLException
end

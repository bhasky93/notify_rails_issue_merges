Web::Application.draw_routes do
  get '/', &SubscriptionsController[:index]

  post '/subscribe/:email', &SubscriptionsController[:create]

  get '/verify/:token', &VerificationsController[:update]
end

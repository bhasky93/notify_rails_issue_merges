$(document).ready(function() {
  $('#subscribe').click(function() {
    var email = $('#email').val();
    if(email != "") {
      request = $.post('/subscribe/' + email)
      request.done(function(data) {
        alert(data['message']);
      })
      request.fail(function(data) {
        console.log("Something went wrong");
      })
    } else {
      alert("Please enter your email id");
    }
  })
})

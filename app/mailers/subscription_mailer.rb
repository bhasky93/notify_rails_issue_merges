class SubscriptionMailer < Web::Application::BaseMailer
  mailer_path File.expand_path('../../views/subscription/', __FILE__)

  mailer def subscription_mail(email, merged_issues)
    @merged_issues = merged_issues
    mail({
      to: email,
      html_body: haml(:subscription_mail)
    })
  end

  mailer def verification_mail(user)
    @user = user
    mail({
      to: @user.email,
      html_body: haml(:verification_mail)
    })
  end
end

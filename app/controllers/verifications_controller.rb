class VerificationsController < Web::Application::BaseController
  action :update do
    user = User.where(token: params[:token]).first
    if !user.nil? && user.genuine?(params[:token])
      user.send_newsletter
      @message = "Thank you for subscribing. You will recieve the list of merged rails issues in a minute. Happy open source contribution"
    else
      @message = "This is an invalid verification link"
    end
    haml :'verifications/update'
  end
end

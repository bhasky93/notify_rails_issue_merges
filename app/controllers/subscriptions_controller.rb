class SubscriptionsController < Web::Application::BaseController
  action :index do
    haml :'subscriptions/index'
  end

  action :create do
    user = User.fetch_or_create(params[:email])
    if user.verified?
        json message: "Thank you! But you are already subscribed"
    elsif user.valid?
      user.subscribe_to_newsletter
      json message: "Hey #{params[:email]}, you are now subcribed successfully to the newsletter, please check your inbox and verify your email address"
    else
      json message: "Enter a valid email"
    end
  end
end
